# TicTacToe




## About

This is a git repository for a Unity and Networking project which was made for a final paper at the university of FER in Zagreb. The repository contains the project and all it's assets, a build version of the game, the final paper and a template of the project so you can do it on your own.

## Template

The template of the project is available on the template branch.

## Download the project

```
cd existing_repo
git clone https://gitlab.com/luka-cicak/tictactoe.git
```

## Necessary tools

- Git
- Unity
- Visual Studio

