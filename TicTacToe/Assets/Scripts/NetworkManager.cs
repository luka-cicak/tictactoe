using System;
using System.Net.Sockets;
using AssemblyCSharp.Assets.Scripts;


public abstract class NetworkManager : IDisposable
{
    protected struct Constants
    {
        public const Int32 PORT = 13000;
    }


    protected GameManager gameManager;
    protected TcpClient tcpClient;
    protected UdpClient udpClient;

    protected NetworkManager(GameManager manager)
    {
        gameManager = manager;
    }


    public abstract void StartNetworkManager();


    public void Dispose()
    {
        if (udpClient != null)
            udpClient.Close();

        if (tcpClient != null)
        {
            byte[] responseBuffer = new byte[256];
            NetworkStream networkStream = tcpClient.GetStream();

            ProtocolData respHeader = new()
            {
                messageCode = ProtocolData.MessageCode.EXIT,
                space = ProtocolData.MoveSpace.NULL_SPACE
            };

            Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.messageCode), 0, responseBuffer, 0, 4);
            Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.space), 0, responseBuffer, 4, 4);

            networkStream.Write(responseBuffer, 0, 8);
        }
    }


    public void SendMove(int move)
    {
        gameManager.DisableBoard();
        byte[] buffer = new byte[256];
        NetworkStream networkStream = tcpClient.GetStream();

        ProtocolData reqHeader = new() { };
        reqHeader.messageCode = ProtocolData.MessageCode.MOVE;
        reqHeader.space = (ProtocolData.MoveSpace)move;

        Buffer.BlockCopy(BitConverter.GetBytes((int)reqHeader.messageCode), 0, buffer, 0, 4);
        Buffer.BlockCopy(BitConverter.GetBytes((int)reqHeader.space), 0, buffer, 4, 4);

        networkStream.Write(buffer, 0, 8);
    }


    public void Restart()
    {
        byte[] buffer = new byte[256];
        NetworkStream networkStream = tcpClient.GetStream();

        ProtocolData reqHeader = new() { };
        reqHeader.messageCode = ProtocolData.MessageCode.RESTART;
        reqHeader.space = ProtocolData.MoveSpace.NULL_SPACE;

        Buffer.BlockCopy(BitConverter.GetBytes((int)reqHeader.messageCode), 0, buffer, 0, 4);
        Buffer.BlockCopy(BitConverter.GetBytes((int)reqHeader.space), 0, buffer, 4, 4);

        networkStream.Write(buffer, 0, 8);
    }
}