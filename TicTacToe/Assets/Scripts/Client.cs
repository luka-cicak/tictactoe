using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using AssemblyCSharp.Assets.Scripts;

public class Client : NetworkManager
{
    public Client(GameManager manager) : base(manager) { }


    public override void StartNetworkManager()
    {
        ConnectClient();
    }


    public async void ConnectClient()
    {
        byte[] buffer = new byte[256];

        try
        {
            IPAddress serverAddress = await Task.Run(() => SendBroadcast());

            if (serverAddress != null)
            {
                tcpClient = new TcpClient();
                await tcpClient.ConnectAsync(serverAddress, Constants.PORT);

                NetworkStream networkStream = tcpClient.GetStream();
                int read;
                SendSync(networkStream, buffer);

                bool close = false;
                while (!close && (read = await networkStream.ReadAsync(buffer)) != 0)
                {

                    ProtocolData recievedUnit = new() { };
                    recievedUnit.messageCode = (ProtocolData.MessageCode)BitConverter.ToInt32(buffer, 0);
                    recievedUnit.space = (ProtocolData.MoveSpace)BitConverter.ToInt32(buffer, 4);

                    ProtocolData respHeader = new() { };
                    byte[] responseBuff = new byte[256];

                    switch (recievedUnit.messageCode)
                    {
                        case ProtocolData.MessageCode.SYNC:
                            {
                                respHeader.messageCode = ProtocolData.MessageCode.TURN;
                                respHeader.space = ProtocolData.MoveSpace.NULL_SPACE;

                                Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.messageCode), 0, responseBuff, 0, 4);
                                Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.space), 0, responseBuff, 4, 4);

                                networkStream.Write(responseBuff, 0, 8);
                                break;
                            }

                        case ProtocolData.MessageCode.TURN:
                            {
                                int result = BitConverter.ToInt32(buffer, 8);

                                gameManager.BeginGame();

                                if (result == 1)
                                {
                                    gameManager.EnableBoard();
                                }

                                break;
                            }

                        case ProtocolData.MessageCode.MOVE:
                            {
                                gameManager.ExecuteMove((int)recievedUnit.space);
                                gameManager.EnableBoard();
                                break;
                            }
                        case ProtocolData.MessageCode.RESTART:
                            {
                                gameManager.InitializeBoard();
                                gameManager.BeginGame();
                                SendSync(networkStream, buffer);
                                break;
                            }
                        case ProtocolData.MessageCode.EXIT:
                            {
                                networkStream.Close();
                                close = true;
                                break;
                            }
                    }
                }
            }
            
        }
        catch (SocketException exc)
        {
            Debug.Log("SocketException caught in ConnectClient: " + exc.Message);
        }
        finally
        {
            if (udpClient != null)
            {
                udpClient.Close();
            }

            if (tcpClient != null)
            {
                tcpClient.Close();
            }

            SceneManager.LoadScene(0);
        }
    }


    private IPAddress SendBroadcast()
    {
        var requestData = Encoding.ASCII.GetBytes("TACTOE");
        var serverEp = new IPEndPoint(IPAddress.Any, 0);
        byte[] responseData = null;

        udpClient = new UdpClient();
        udpClient.EnableBroadcast = true;
        udpClient.Client.ReceiveTimeout = 5000;

        while (true)
        {
            if (udpClient.Client != null)
            {

                udpClient.Send(requestData, requestData.Length, new IPEndPoint(IPAddress.Broadcast, Constants.PORT));

                Task.Run(() =>
                    {
                        try
                        {
                            Debug.Log("Sending Broadcast");
                            responseData = udpClient.Receive(ref serverEp);
                        }
                        catch (SocketException exc)
                        {
                            Debug.Log("SocketException caught in SendBroadcast, udpClient was closed or timed out: " + exc.Message);
                        }
                    }
                ).Wait(5000);

                if (responseData != null && Encoding.ASCII.GetString(responseData) == "TIC")
                {
                    udpClient.Close();
                    return serverEp.Address;
                }
            }
            else
                break;
        }

        return null;
    }


    private void SendSync(NetworkStream networkStream, byte[] buffer)
    {
        ProtocolData reqHeader = new() { };
        reqHeader.messageCode = ProtocolData.MessageCode.SYNC;
        reqHeader.space = ProtocolData.MoveSpace.NULL_SPACE;

        Buffer.BlockCopy(BitConverter.GetBytes((int)reqHeader.messageCode), 0, buffer, 0, 4);
        Buffer.BlockCopy(BitConverter.GetBytes((int)reqHeader.space), 0, buffer, 4, 4);

        networkStream.Write(buffer, 0, 8);
    }
}
