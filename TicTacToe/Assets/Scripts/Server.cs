using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using AssemblyCSharp.Assets.Scripts;

public class Server : NetworkManager
{
    private TcpListener tcpServer;


    public Server(GameManager manager) : base(manager) { }


    public override void StartNetworkManager()
    {
        StartListener();
    }


    public async void StartListener()
    {
        byte[] buffer = new byte[256];

        try
        {
            IPAddress myIpAddress = GetLocalIPAddress();
            tcpServer = new TcpListener(myIpAddress, Constants.PORT);

            if (myIpAddress != null)
            {
                await Task.Run(() => ListenForBroadcast());
                NetworkStream networkStream = tcpClient.GetStream();
                int read;

                bool close = false;
                while (!close && (read = await networkStream.ReadAsync(buffer)) != 0)
                {
                    ProtocolData recievedUnit = new() { };
                    recievedUnit.messageCode = (ProtocolData.MessageCode)BitConverter.ToInt32(buffer, 0);
                    recievedUnit.space = (ProtocolData.MoveSpace)BitConverter.ToInt32(buffer, 4);

                    ProtocolData respHeader = new() { };
                    byte[] responseBuff = new byte[256];

                    switch (recievedUnit.messageCode)
                    {
                        case ProtocolData.MessageCode.SYNC:
                            {
                                respHeader.messageCode = ProtocolData.MessageCode.SYNC;
                                respHeader.space = ProtocolData.MoveSpace.NULL_SPACE;

                                Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.messageCode), 0, responseBuff, 0, 4);
                                Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.space), 0, responseBuff, 4, 4);

                                networkStream.Write(responseBuff, 0, 8);
                                break;
                            }

                        case ProtocolData.MessageCode.TURN:
                            {
                                respHeader.messageCode = ProtocolData.MessageCode.TURN;
                                respHeader.space = ProtocolData.MoveSpace.NULL_SPACE;

                                Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.messageCode), 0, responseBuff, 0, 4);
                                Buffer.BlockCopy(BitConverter.GetBytes((int)respHeader.space), 0, responseBuff, 4, 4);

                                System.Random rnd = new();
                                int result = rnd.Next() % 2;
                                Buffer.BlockCopy(BitConverter.GetBytes(result), 0, responseBuff, 8, 4);

                                gameManager.BeginGame();

                                if (result == 0)
                                {
                                    gameManager.EnableBoard();
                                }

                                networkStream.Write(responseBuff, 0, 12);
                                break;
                            }

                        case ProtocolData.MessageCode.MOVE:
                            {
                                gameManager.ExecuteMove((int)recievedUnit.space);
                                gameManager.EnableBoard();
                                break;
                            }
                        case ProtocolData.MessageCode.EXIT:
                            {
                                networkStream.Close();
                                close = true;
                                break;
                            }
                    }
                }
            }
        }
        catch (SocketException exc)
        {
            Debug.Log("SocketException from StartListener: " + exc.Message);
        }
        finally
        {
            if (udpClient != null)
            {
                udpClient.Close();
            }

            if (tcpClient != null)
            {
                tcpClient.Close();
            }

            if (tcpServer != null)
            {
                tcpServer.Stop();
            }

            SceneManager.LoadScene(0);
        }
    }


    IPAddress GetLocalIPAddress()
    {
        IPAddress localAddress = null;

        try
        {
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0);

            s.Connect("8.8.8.8", 65530);

            if (s.LocalEndPoint is IPEndPoint endPoint)
                localAddress = endPoint.Address;
        }
        catch (SocketException exc)
        {
            Debug.Log("SocketException from GetLocalIPAddress: " + exc.Message);
        }

        return localAddress;
    }


    void ListenForBroadcast()
    {
        var responseData = Encoding.ASCII.GetBytes("TIC");
        var clientEp = new IPEndPoint(IPAddress.Any, 0);

        udpClient = new UdpClient(Constants.PORT);

        while (true)
        {
            var requestData = udpClient.Receive(ref clientEp);

            if (Encoding.ASCII.GetString(requestData) == "TACTOE")
            {
                tcpServer.Start();
                udpClient.Send(responseData, responseData.Length, clientEp);
                tcpClient = tcpServer.AcceptTcpClient();
                break;
            }
        }

        udpClient.Close();
    }
}
